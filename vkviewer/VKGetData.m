//
//  VKGetData.m
//  vkviewer
//
//  Created by Алексей on 17.03.16.
//  Copyright © 2016 kirovalexey. All rights reserved.
//

#import "VKGetData.h"

@implementation VKGetData

-(void)getFriends{
    VKRequest *friends = [[VKApi friends] get:[NSDictionary dictionaryWithObjectsAndKeys:@"fields", @"city, universities, nickname", nil]];
    [friends executeWithResultBlock:^(VKResponse *response) {
//        NSLog(@"Json result: %@", ((VKUsersArray*)response.parsedModel).items);
        NSString *user_ids = [((VKUsersArray*)response.parsedModel).items componentsJoinedByString:@", "];
        NSString *fields = @"city, education, photo_100";
        VKRequest *users = [[VKApi users] get:@{@"user_ids" : user_ids, @"fields":fields}];
        [users executeWithResultBlock:^(VKResponse *response) {
//            NSLog(@"Json result: %@", response.json);
            NSMutableArray *usersArray = [[NSMutableArray alloc] init];
            for(VKUser *row in ((VKUsersArray*)response.parsedModel).items){
                NSDictionary *fields = @{@"id":row.id,
                                         @"firstname":row.first_name ? row.first_name : @"",
                                         @"lastname":row.last_name ? row.last_name : @"",
                                         @"city":row.city.title ? row.city.title : @"",
                                         @"univercity":row.university_name ? row.university_name : @"",
                                         @"avatar":row.photo_100 ? row.photo_100 : @""};
                [usersArray addObject:fields];
            }
            [self.delegate getFriendsComplete:usersArray];
        } errorBlock:^(NSError * error) {
            if (error.code != VK_API_ERROR) {
                [error.vkError.request repeat];
            } else {
                NSLog(@"VK error: %@", error);
            }
        }];
    } errorBlock:^(NSError * error) {
        if (error.code != VK_API_ERROR) {
            [error.vkError.request repeat];
        } else {
            NSLog(@"VK error: %@", error);
        }
    }];
}

-(void)getGroups:(NSString*) user_id{
    VKRequest *user_groups = [VKRequest requestWithMethod:@"groups.get" parameters: [NSDictionary dictionaryWithObjectsAndKeys:user_id, @"user_id", @"1", @"extended", @"1000", @"count", nil]];
    [user_groups executeWithResultBlock:^(VKResponse *response) {
        [self.delegate getGroupsComplete:[response.json objectForKey:@"items"]];
    }errorBlock:^(NSError *error){
        if (error.code != VK_API_ERROR) {
            [error.vkError.request repeat];
        } else {
            NSLog(@"VK error: %@", error);
        }
    }];
}

@end
