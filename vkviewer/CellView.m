//
//  CellView.m
//  vkviewer
//
//  Created by Алексей on 19.03.16.
//  Copyright © 2016 kirovalexey. All rights reserved.
//

#import "CellView.h"

@implementation CellView

-(id)initWithName:(NSString*) firstname lastName:(NSString*) lastname city:(NSString*) city univercity:(NSString*) univercity avatar:(NSString *)avatar{
    self = [super init];
    
    if(self){
        self.firstname = firstname;
        self.lastname = lastname;
        self.city = city;
        self.univercity = univercity;
        self.avatar = avatar;
    }
    
    return self;
}

-(id)initWithUserFields:(NSDictionary *)user_fields{
    self = [super init];
    
    if(self){
        self.firstname = [user_fields objectForKey:@"firstname"];
        self.lastname = [user_fields objectForKey:@"lastname"];
        self.city = [user_fields objectForKey:@"city"];
        self.univercity = [user_fields objectForKey:@"univercity"];
        self.avatar = [user_fields objectForKey:@"avatar"];
    }
    
    return self;
}

-(void)drawRect:(CGRect)rect{
    UILabel *name = [[UILabel alloc] init];
    UILabel *city = [[UILabel alloc] init];
    UILabel *univercity = [[UILabel alloc] init];
    UIImageView *avatar = [[UIImageView alloc] init];
    
    name.translatesAutoresizingMaskIntoConstraints = NO;
    city.translatesAutoresizingMaskIntoConstraints = NO;
    univercity.translatesAutoresizingMaskIntoConstraints = NO;
    avatar.translatesAutoresizingMaskIntoConstraints = NO;
    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor whiteColor];
    
    name.text  = [NSString stringWithFormat:@"%@ %@", self.firstname, self.lastname];
    city.text       = self.city;
    univercity.text = self.univercity;
    
    avatar.image = [UIImage imageNamed:@"icn_default"];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.avatar]];
        UIImage *image = [UIImage imageWithData:data];
        dispatch_async(dispatch_get_main_queue(), ^{
            avatar.image = image;
        });  
    });
//    NSData *imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:self.avatar]];
    
    [self addSubview:name];
    [self addSubview:city];
    [self addSubview:univercity];
    [self addSubview:avatar];
    
    UIView *content = self;
    
    NSDictionary *views = NSDictionaryOfVariableBindings(name, city, univercity, content, avatar);
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[avatar(80)]-[name]-|" options:0 metrics:nil views:views]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[avatar]-[city]-|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[avatar]-[univercity]-|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[name]-[city]-[univercity]" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[avatar(80)]-|" options:0 metrics:nil views:views]];
}

@end
