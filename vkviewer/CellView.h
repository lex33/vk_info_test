//
//  CellView.h
//  vkviewer
//
//  Created by Алексей on 19.03.16.
//  Copyright © 2016 kirovalexey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellView : UITableViewCell

-(id)initWithName:(NSString*) firstname lastName:(NSString*) lastname city:(NSString*) city univercity:(NSString*) univercity avatar:(NSString*) avatar;

-(id)initWithUserFields:(NSDictionary*) user_fields;

@property (strong, nonatomic) NSString* lastname;
@property (strong, nonatomic) NSString* firstname;
@property (strong, nonatomic) NSString* city;
@property (strong, nonatomic) NSString* univercity;
@property (strong, nonatomic) NSString* avatar;

@property (strong, nonatomic) NSDictionary* user_fields;

@end
