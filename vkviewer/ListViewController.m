//
//  ListViewController.m
//  vkviewer
//
//  Created by Алексей on 19.03.16.
//  Copyright © 2016 kirovalexey. All rights reserved.
//

#import "ListViewController.h"
#import "CellView.h"
#import "DetailViewController.h"

@implementation ListViewController

UITableView *table;
NSMutableArray* friendsList;
NSMutableArray* sourceFriendsList;
UITextField *search_string;
VKGetData *get;

-(void)viewDidLoad{
    self.navigationController.navigationBar.translucent = NO;
    UIView *root = [[UIView alloc] init];
    [root setBackgroundColor:[UIColor blueColor]];
    
    table = [[UITableView alloc] initWithFrame:CGRectZero];
    [table setDataSource:self];
    [table setDelegate:self];
    [table setBackgroundColor:[UIColor whiteColor]];
    table.translatesAutoresizingMaskIntoConstraints = NO;
    
    //search views
    UIView *search_bg = [[UIView alloc] init];
    search_bg.translatesAutoresizingMaskIntoConstraints = NO;
    search_bg.backgroundColor = [UIColor grayColor];
    
    search_string = [[UITextField alloc] init];
    search_string.translatesAutoresizingMaskIntoConstraints = NO;
    search_string.backgroundColor = [UIColor whiteColor];
    search_string.delegate = self;
    search_string.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    UIButton *search_btn = [[UIButton alloc] init];
    search_btn.translatesAutoresizingMaskIntoConstraints = NO;
    search_btn.backgroundColor = [UIColor blueColor];
    [search_btn setTitle:@"Find" forState:UIControlStateNormal];
    [search_btn addTarget:self action:@selector(searchBtnPress:) forControlEvents:UIControlEventTouchUpInside];
    
    [search_bg addSubview:search_string];
    [search_bg addSubview:search_btn];
    [root addSubview:search_bg];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    //end search views
    
    [root addSubview:table];
    
    self.view = root;
    
    id topGuide = self.topLayoutGuide;
    
    NSDictionary *views = NSDictionaryOfVariableBindings(root, table, topGuide, search_bg, search_btn, search_string);
    
    [root addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[search_bg]|" options:0 metrics:nil views:views]];
    
    [root addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[search_string]-[search_btn(50)]-|" options:NSLayoutFormatAlignAllCenterY metrics:nil views:views]];
    
    [root addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[search_btn]-|" options:0 metrics:nil views:views]];
    
    [root addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[search_string(==search_btn)]" options:0 metrics:nil views:views]];
    
    [root addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[topGuide][search_bg][table]|" options:0 metrics:nil views:views]];
    
    [root addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[table]|" options:0 metrics:nil views:views]];
    
//    table.estimatedRowHeight = 200;
    table.rowHeight = 100;
    
    [self setupRefresh];
    
    get = [[VKGetData alloc] init];
    get.delegate = self;
    [get getFriends];
}


-(int)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return (int)friendsList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *cur_user = [friendsList objectAtIndex:indexPath.row];
//    CellView *cell = [[CellView alloc] initWithName:cur_user.first_name lastName:cur_user.last_name city:cur_user.city.title  univercity:cur_user.university_name avatar:cur_user.photo_100];
    CellView *cell = [[CellView alloc] initWithUserFields:cur_user];
    return cell;
}


-(void)setupRefresh{
    UIRefreshControl *refresh = [[UIRefreshControl alloc] init];
    refresh.tintColor = [UIColor blueColor];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
    [refresh addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [table addSubview:refresh];
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    // Do your job, when done:
    [get getFriends];
    [refreshControl endRefreshing];
}

-(void)getFriendsComplete:(NSMutableArray *)friends{
    friendsList = friends;
    sourceFriendsList = [friends mutableCopy];
    [table reloadData];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [search_string resignFirstResponder];
    DetailViewController *user_detail = [[DetailViewController alloc] initWithUserFields:[friendsList objectAtIndex:indexPath.row]];
    user_detail.title = [[friendsList objectAtIndex:indexPath.row] objectForKey:@"firstname"];
    [self.navigationController pushViewController:user_detail animated:YES];
}

-(void)searchFriends:(NSString*)string{
    if(![string  isEqual: @""]){
        [friendsList removeAllObjects];
        for(NSDictionary *friend in sourceFriendsList){
            if([[friend objectForKey:@"firstname"] localizedStandardContainsString:string] ||
               [[friend objectForKey:@"lastname"] localizedStandardContainsString:string] ||
               [[friend objectForKey:@"city"] localizedStandardContainsString:string] ||
               [[friend objectForKey:@"univercity"] localizedStandardContainsString:string]){
                [friendsList addObject:friend];
            }
        }
    }else{
        friendsList = [sourceFriendsList mutableCopy];
    }
    [table reloadData];
}

-(void)searchBtnPress:(id)sender{
    [self searchFriends:search_string.text];
}

-(void)dismissKeyboard{
    [search_string resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    [self searchFriends:textField.text];
    return YES;
}

@end
