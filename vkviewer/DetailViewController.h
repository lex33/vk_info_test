//
//  DetailViewController.h
//  vkviewer
//
//  Created by Алексей on 17.03.16.
//  Copyright © 2016 kirovalexey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VKGetData.h"

@interface DetailViewController : UIViewController<VKGetDataDelegate, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSDictionary* user_fields;

-(id)initWithUserFields:(NSDictionary*) user_fields;

@end
