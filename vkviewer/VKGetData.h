//
//  VKGetData.h
//  vkviewer
//
//  Created by Алексей on 17.03.16.
//  Copyright © 2016 kirovalexey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <VK-ios-sdk/VKSdk.h>

@protocol VKGetDataDelegate <NSObject>

@optional
-(void)getFriendsComplete:(NSMutableArray*) friends;
-(void)getGroupsComplete:(NSMutableArray*) groups;
@end


@interface VKGetData : NSObject
@property (nonatomic, weak) id <VKGetDataDelegate> delegate;
-(void)getFriends;
-(void)getGroups:(NSString*) user_id;
@end
