//
//  DetailViewController.m
//  vkviewer
//
//  Created by Алексей on 17.03.16.
//  Copyright © 2016 kirovalexey. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

UITableView *group_table;
NSMutableArray *user_groups;

-(id)initWithUserFields:(NSDictionary *)user_fields{
    self = [super init];
    
    if(self){
        self.user_fields = user_fields;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
    UIView *topView = [[UIView alloc] init];
    UIImageView *avatar = [[UIImageView alloc] init];
    UILabel *name = [[UILabel alloc] init];
    group_table = [[UITableView alloc] initWithFrame:CGRectZero];
    
    topView.translatesAutoresizingMaskIntoConstraints = NO;
    topView.backgroundColor = [UIColor whiteColor];
    
    avatar.translatesAutoresizingMaskIntoConstraints = NO;
    name.translatesAutoresizingMaskIntoConstraints = NO;
    group_table.translatesAutoresizingMaskIntoConstraints = NO;
    group_table.rowHeight = 50;
    group_table.backgroundColor = [UIColor whiteColor];
    
    group_table.delegate = self;
    group_table.dataSource = self;
    
    name.text  = [NSString stringWithFormat:@"%@ %@", [self.user_fields objectForKey:@"firstname"] , [self.user_fields objectForKey:@"lastname"]];
    
    NSData *imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:[self.user_fields objectForKey:@"avatar"]]];
    avatar.image = [UIImage imageWithData:imageData];
    
    [topView addSubview:name];
    [topView addSubview:avatar];
    [self.view addSubview:group_table];
    
    [self.view addSubview:topView];
    
    NSDictionary *views = @{@"avatar":avatar, @"name":name, @"topView":topView, @"table":group_table};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[topView]|" options: 0 metrics:nil views:views]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[table]|" options: 0 metrics:nil views:views]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[avatar(80)]-[name]-|" options: NSLayoutFormatAlignAllCenterY metrics:nil views:views]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[avatar(80)]-|" options: 0 metrics:nil views:views]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[topView][table]|" options: 0 metrics:nil views:views]];
}

-(void)viewDidAppear:(BOOL)animated{
    VKGetData *get = [[VKGetData alloc] init];
    get.delegate = self;
    [get getGroups:[self.user_fields objectForKey:@"id"]];
}

-(void)getGroupsComplete:(NSMutableArray *)groups{
    user_groups = groups;
    [group_table reloadData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return user_groups.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"group_cell"];
    cell.textLabel.text = [user_groups[indexPath.row] objectForKey:@"name"];
    return cell;
}

@end
