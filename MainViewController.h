//
//  MainViewController.h
//  vkviewer
//
//  Created by Алексей on 17.03.16.
//  Copyright © 2016 kirovalexey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <VK-ios-sdk/VKSdk.h>

@interface MainViewController : UIViewController<VKSdkUIDelegate, VKSdkDelegate>

@end
