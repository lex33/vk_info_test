//
//  MainViewController.m
//  vkviewer
//
//  Created by Алексей on 17.03.16.
//  Copyright © 2016 kirovalexey. All rights reserved.
//

#import "MainViewController.h"
#import "DetailViewController.h"
#import <VK-ios-sdk/VKSdk.h>
#import "ListViewController.h"


@interface MainViewController ()

@end

@implementation MainViewController

VKSdk *vkInstance;
NSArray *scope;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}

- (void)loadView {
    CGRect appFrame = [UIScreen mainScreen].bounds;
    UIView *mainView = [[UIView alloc] initWithFrame:appFrame];
    [mainView setBackgroundColor:[UIColor whiteColor]];
    
    UIButton *authBtn = [[UIButton alloc] init];
    authBtn.translatesAutoresizingMaskIntoConstraints = NO;
    authBtn.backgroundColor = [UIColor blueColor];
    authBtn.titleLabel.textColor = [UIColor whiteColor];
    [authBtn setTitle:@"Push me" forState:UIControlStateNormal];
    [authBtn addTarget:self action:@selector(btnPushed) forControlEvents:UIControlEventTouchUpInside];
    
    [mainView addSubview:authBtn];
    
    [mainView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[mainView]-(<=1)-[authBtn(50)]" options:NSLayoutFormatAlignAllCenterX metrics:nil views:NSDictionaryOfVariableBindings(authBtn, mainView)]];
    
    [mainView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[mainView]-(<=1)-[authBtn(200)]" options:NSLayoutFormatAlignAllCenterY metrics:nil views:NSDictionaryOfVariableBindings(authBtn, mainView)]];
    
    self.view = mainView;
    self.navigationItem.title = @"Авторизация";
}

- (void)viewWillAppear:(BOOL)animated{
    scope = @[@"friends", @"groups"];
    vkInstance = [VKSdk initializeWithAppId:@"5364040"];
    [vkInstance setUiDelegate:self];
    [vkInstance registerDelegate:self];
    
    [VKSdk wakeUpSession:scope completeBlock:^(VKAuthorizationState state, NSError *error){
        if (state == VKAuthorizationAuthorized){
            [self openList];
        }else if (error){
            
        }
    }];
}

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller {
    [self presentViewController:controller animated:YES completion:nil];
}

-(void)vkSdkUserAuthorizationFailed{
    NSLog(@"failed");
}

-(void)vkSdkNeedCaptchaEnter:(VKError *)captchaError{
    NSLog(@"captcha");
}

-(void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result{
//    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:list];
}

-(void)btnPushed{
//    DetailViewController *second = [[DetailViewController alloc] init];
    [VKSdk authorize:scope];
//    [self openList];
}

-(void)openList{
    ListViewController *list = [[ListViewController alloc] init];
    list.title = @"Друзья";
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:list];
    [self presentViewController:nav animated:YES completion:nil];
}

@end
