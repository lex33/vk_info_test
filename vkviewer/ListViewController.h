//
//  ListViewController.h
//  vkviewer
//
//  Created by Алексей on 19.03.16.
//  Copyright © 2016 kirovalexey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VKGetData.h"

@interface ListViewController : UITableViewController<UITableViewDataSource, UITableViewDelegate, VKGetDataDelegate, UITextFieldDelegate>

@end
